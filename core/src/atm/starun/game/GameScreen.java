package atm.starun.game;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import atm.starun.game.helpers.AssetLoader;
import atm.starun.game.helpers.Hud;
import atm.starun.game.helpers.ProjectGestureListener;
import atm.starun.game.helpers.ScreenShaker;

public class GameScreen implements Screen {
	final Starun game;

	public static Sprite starSprite;
	public static OrthographicCamera camera;
	private long lastAsteroidTime, startTime, elapsedSecondsSinceStart;
	public static Vector3 touchPos;
	public static Integer score;
	private int rotDegree;
	public static Integer collisions;
	public static float acceleration, asteroidVel;
	public static Rectangle touchArea;
	// These are the x coordinates the asteroids will spawn on.
	private int[] asteroidPos = { 5, 90, 180, 270 };
	private Hud hud;
	private Array<Circle> asteroidsCirc;
	private Circle starCircle;
	private ScreenShaker shaker;

	public GameScreen(final Starun game) {
		this.game = game;

		// Initialize objects
		touchPos = new Vector3();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT);
		score = 0;
		rotDegree = 0;
		acceleration = 0;
		asteroidVel = 0;
		collisions = 0;
		startTime = TimeUtils.millis();
		hud = new Hud(game.batch);
		shaker = new ScreenShaker(camera);

		Gdx.input.setInputProcessor(new GestureDetector(new ProjectGestureListener()));

		// Initialize sprites
		starSprite = new Sprite(AssetLoader.starImage);
		starSprite.setSize(67, 67);
		starSprite.setOrigin(starSprite.getWidth() / 2, starSprite.getHeight() / 2);
		starSprite.setPosition(Starun.SCREEN_WIDTH / 2, 50);

		// Initialize boxes.
		// Subtract 12 to make the game easier.
		starCircle = new Circle(starSprite.getX(), starSprite.getY(), (starSprite.getWidth() / 2) - 12);
		asteroidsCirc = new Array<Circle>();
		// The touch area admits every position that is on top of the star, and
		// its width is slightly greater than the sprite to make it easier to
		// drag.
		touchArea = new Rectangle(starSprite.getX(), starSprite.getY() - 100, starSprite.getWidth() + 50,
				starSprite.getHeight() + Starun.SCREEN_HEIGHT);
	}

	private void spawnAsteroid() {
		// Create asteroid at a certain position and set its properties.
		Circle asteroidCirc = new Circle();
		asteroidCirc.x = asteroidPos[MathUtils.random(3)];
		asteroidCirc.y = Starun.SCREEN_HEIGHT;
		asteroidCirc.radius = 46;
		asteroidsCirc.add(asteroidCirc);
		// Store spawn time
		lastAsteroidTime = TimeUtils.nanoTime();
	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		// Clear OpenGL screen
		Gdx.gl.glClearColor(23 / 255f, 5 / 255f, 64 / 255f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		update(delta);

		// Initialize batch
		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();
		Starun.drawBackground(game.batch, 10);
		starSprite.draw(game.batch);
		// Draw asteroids
		for (Circle asteroidCirc : asteroidsCirc) {
			game.batch.draw(AssetLoader.asteroidImage, asteroidCirc.x, asteroidCirc.y, asteroidCirc.radius * 2,
					asteroidCirc.radius * 2);
		}
		// End batch and draw HUD
		game.batch.end();
		hud.stage.draw();
	}

	private void update(float delta) {
		elapsedSecondsSinceStart = (TimeUtils.timeSinceMillis(startTime) / 1000);

		// Handle star rotation and hitbox updating
		starSprite.setRotation(rotDegree);
		rotDegree += 3;
		updateBoxes();

		// update hud and camera
		camera.update();
		hud.update(delta);

		// Handle PC-Input. (Android input handled in ProjectGestureListener)
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			starSprite.setX(starSprite.getX() - 500 * Gdx.graphics.getDeltaTime());
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			starSprite.setX(starSprite.getX() + 500 * Gdx.graphics.getDeltaTime());
		}

		// Asteroids spawn.
		if (TimeUtils.nanoTime() - lastAsteroidTime > 450000000 && elapsedSecondsSinceStart > 2) {
			spawnAsteroid();
		}
		// Screen limit handling (only for PC version, Android version handled
		// in ProjectGestureListener)
		if (starSprite.getX() < 0) {
			starSprite.setX(0);
		}
		if (starSprite.getX() > Starun.SCREEN_WIDTH - 75) {
			starSprite.setX(Starun.SCREEN_WIDTH - 75);
		}
		// Asteroids iterator and life checker
		iterateAsteroids();
		checkCollisions();

		// Update screen shaker
		shaker.update(camera, delta);
	}

	private void iterateAsteroids() {
		// The asteroids velocity have acceleration (kind of).
		asteroidVel = (600 * Gdx.graphics.getDeltaTime()) + acceleration;
		Iterator<Circle> asteroidsCircIterator = asteroidsCirc.iterator();
		while (asteroidsCircIterator.hasNext()) {
			Circle asteroidCirc = asteroidsCircIterator.next();
			asteroidCirc.setY(asteroidCirc.y - asteroidVel);
			if (acceleration < 15.0) {
				acceleration += 0.0017;
			} else {
				acceleration = 15;
			}
			if (asteroidCirc.y + 100 < 0) {
				asteroidsCircIterator.remove();
				score++;
			}
			// Handle collisions
			if (asteroidCirc.overlaps(starCircle)) {
				AssetLoader.hitSound.play();
				collisions++;
				asteroidsCircIterator.remove();
				shaker.shake(0.25f);
			}
		}
	}

	private void checkCollisions() {
		// End game
		if (collisions >= 3) {
			if (score > Starun.previousHighScore) {
				Starun.setHighScore(score);
			}
			AssetLoader.gameMusic.stop();
			AssetLoader.menuMusic.play();
			game.setScreen(new GameOverScreen(game));
			dispose();
		}
	}

	private void updateBoxes() {
		// Star container
		starCircle.setX(starSprite.getX());
		starCircle.setY(starSprite.getY());

		touchArea.setX(starSprite.getX());
		touchArea.setY(starSprite.getY() - 100);
	}

	// Methods not used that need to be overridden.
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

}
