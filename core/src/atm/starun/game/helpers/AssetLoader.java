package atm.starun.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

import atm.starun.game.Starun;

public class AssetLoader {
	public SpriteBatch batch;
	public static Skin starunskin;
	public static FreeTypeFontGenerator generator;
	public static FreeTypeFontParameter parameter;
	public static BitmapFont titleFont, textFont, recordFont, creditFont;
	public static Label.LabelStyle titleFontStyle, textFontStyle, recordFontStyle, creditFontStyle;
	public static Preferences prefs;
	public static Sound newRecordSound, playAgainSound, mainMenuSound, hitSound;
	public static Music menuMusic, gameMusic;
	public static TextureAtlas starunAtlas;
	public static AtlasRegion starImage, asteroidImage, backgroundImage;
	public static Array<Sprite> backgroundArray;

	public AssetLoader() {

	}

	public void loadFonts() {
		// Generate fonts using ArchitectsDaughter font.
		// TODO use 8-bit font (look Hiero)
		generator = new FreeTypeFontGenerator(Gdx.files.internal("img/SpanishTeletext.ttf"));
		parameter = new FreeTypeFontParameter();

		// Title font parameter
		parameter.size = 42;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLUE;
		parameter.borderWidth = 2;
		parameter.minFilter = TextureFilter.Nearest;
		parameter.magFilter = TextureFilter.Nearest;

		titleFont = generator.generateFont(parameter);

		// Text font parameter
		parameter.size = 24;
		textFont = generator.generateFont(parameter);

		// Record font parameter
		parameter.borderColor = Color.RED;
		recordFont = generator.generateFont(parameter);

		// Credit font parameter
		parameter.size = 11;
		parameter.borderWidth = 0;
		creditFont = generator.generateFont(parameter);

		generator.dispose();

		// With the created fonts, we create the LabelStyles that will be used
		// in Scene2D screens.

		titleFontStyle = new LabelStyle(titleFont, titleFont.getColor());
		textFontStyle = new LabelStyle(textFont, textFont.getColor());
		recordFontStyle = new LabelStyle(recordFont, recordFont.getColor());
		creditFontStyle = new LabelStyle(creditFont, creditFont.getColor());
	}

	public void loadTextures() {
		starunskin = new Skin(Gdx.files.internal("img/starunskin.json"));

		starunAtlas = new TextureAtlas(Gdx.files.internal("img/starun.atlas"));
		starImage = new AtlasRegion(starunAtlas.findRegion("estrellita"));
		asteroidImage = new AtlasRegion(starunAtlas.findRegion("asteroid"));
		backgroundImage = new AtlasRegion(starunAtlas.findRegion("background"));

		// Create the array that'll be used to move the background (See
		// Starun.drawBackground)
		backgroundArray = new Array<Sprite>();
		Sprite bg1 = new Sprite(backgroundImage);
		bg1.setSize(Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT);
		bg1.setY(0);
		backgroundArray.add(bg1);
		Sprite bg2 = new Sprite(backgroundImage);
		bg2.setSize(Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT);
		bg2.setY(Starun.SCREEN_HEIGHT);
		backgroundArray.add(bg2);

	}

	public void loadSounds() {
		// Load music and sounds and set looping of musics to true.
		newRecordSound = Gdx.audio.newSound(Gdx.files.internal("sounds/record.wav"));
		playAgainSound = Gdx.audio.newSound(Gdx.files.internal("sounds/playagain.wav"));
		mainMenuSound = Gdx.audio.newSound(Gdx.files.internal("sounds/mainmenu.wav"));
		hitSound = Gdx.audio.newSound(Gdx.files.internal("sounds/hit.wav"));

		menuMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/menumusic.ogg"));
		gameMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/gamemusic.ogg"));

		menuMusic.setLooping(true);
		gameMusic.setLooping(true);

	}

	public void dispose() {
		starunAtlas.dispose();
		newRecordSound.dispose();
		hitSound.dispose();
		menuMusic.dispose();
		gameMusic.dispose();
		playAgainSound.dispose();
	}

}
