package atm.starun.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

import atm.starun.game.GameScreen;
import atm.starun.game.Starun;

public class ProjectGestureListener implements GestureListener {

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {

		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {

		return false;
	}

	@Override
	public boolean longPress(float x, float y) {

		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {

		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		GameScreen.touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
		GameScreen.camera.unproject(GameScreen.touchPos);
		if (GameScreen.touchArea.contains(GameScreen.touchPos.x, GameScreen.touchPos.y)) {
			GameScreen.starSprite.setX(GameScreen.touchPos.x - (GameScreen.starSprite.getWidth() / 2));
		}
		if (GameScreen.starSprite.getX() < 0) {
			GameScreen.starSprite.setX(0);
		}
		if (GameScreen.starSprite.getX() > Starun.SCREEN_WIDTH - 75) {
			GameScreen.starSprite.setX(Starun.SCREEN_WIDTH - 75);
		}
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {

		return false;
	}

	@Override
	public boolean zoom(float originalDistance, float currentDistance) {

		return false;
	}

	@Override
	public boolean pinch(Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer,
			Vector2 secondPointer) {

		return false;
	}

	@Override
	public void pinchStop() {
	}
}
