package atm.starun.game;

import java.util.Iterator;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import atm.starun.game.helpers.AssetLoader;

public class Starun extends Game {
	public static final int SCREEN_WIDTH = 360;
	public static final int SCREEN_HEIGHT = 640;

	public SpriteBatch batch;
	public static Skin skin;
	public static Preferences prefs;
	public AssetLoader assets;
	public static int previousHighScore;

	@Override
	public void create() {
		batch = new SpriteBatch();
		assets = new AssetLoader();

		assets.loadTextures();
		assets.loadFonts();
		assets.loadSounds();

		loadPreferences();
		previousHighScore = getHighScore();

		this.setScreen(new MainMenuScreen(this));
	}

	public void render() {
		super.render();
	}

	public void dispose() {
		batch.dispose();
		assets.dispose();
	}

	public void loadPreferences() {
		prefs = Gdx.app.getPreferences("Starun");
		if (!prefs.contains("highScore")) {
			prefs.putInteger("highScore", 0);
		}
	}

	public static void setHighScore(int val) {
		prefs.putInteger("highScore", val);
		prefs.flush();
	}

	public static int getHighScore() {
		return prefs.getInteger("highScore");
	}

	public static int getWidth() {
		return SCREEN_WIDTH;
	}

	public static int getHeight() {
		return SCREEN_HEIGHT;
	}

	public static void drawBackground(Batch batch, float a) {
		batch.disableBlending();
		Iterator<Sprite> iter = AssetLoader.backgroundArray.iterator();
		while (iter.hasNext()) {
			Sprite spr = iter.next();
			spr.setY(spr.getY() - a);
			if (spr.getY() < -spr.getHeight()) {
				iter.remove();
				Sprite sprite = new Sprite(AssetLoader.backgroundImage);
				sprite.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
				sprite.setY(SCREEN_HEIGHT);
				AssetLoader.backgroundArray.add(sprite);
			}
		}
		for (Sprite sprite : AssetLoader.backgroundArray) {
			sprite.draw(batch);
		}
		batch.enableBlending();
	}
}
